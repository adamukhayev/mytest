CREATE TABLE tax
(
    id         SERIAL PRIMARY KEY,
    tax_group      CHARACTER VARYING(128) NOT NULL,
    tax_subgroup        CHARACTER VARYING(128) NOT NULL,
    district_of_the_Russian_Federation    CHARACTER VARYING(128) NOT NULL,
    region_of_Russian_Federation CHARACTER VARYING(128),
    year INTEGER ,
    value INTEGER
);

insert into tax(tax_group,tax_subgroup,district_of_the_Russian_Federation,region_of_Russian_Federation,year,value)values ('','Транспортный налог','','Краснодарский край',0,1);
insert into tax(tax_group,tax_subgroup,district_of_the_Russian_Federation,region_of_Russian_Federation,year,value)values ('','Транспортный налог','','Ростовская область',0,2);
insert into tax(tax_group,tax_subgroup,district_of_the_Russian_Federation,region_of_Russian_Federation,year,value)values ('','Земельный налог','','Краснодарский край',0,3);
insert into tax(tax_group,tax_subgroup,district_of_the_Russian_Federation,region_of_Russian_Federation,year,value)values ('','Земельный налог','','Ростовская область',0,4);

