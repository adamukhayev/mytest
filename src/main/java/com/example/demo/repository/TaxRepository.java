package com.example.demo.repository;


import com.example.demo.model.dto.Dtos;
import com.example.demo.model.entity.Entitis;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TaxRepository extends JpaRepository<Entitis,String> {

}
