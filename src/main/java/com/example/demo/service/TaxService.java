package com.example.demo.service;

import org.modelmapper.ModelMapper;
import com.example.demo.model.dto.Dtos;
import com.example.demo.model.entity.Entitis;
import com.example.demo.repository.TaxRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TaxService {
    private final TaxRepository taxRepository;
    private final ModelMapper modelMapper;
    public List<Dtos> getTax(String row, String col) {
        List<Entitis> entitis = taxRepository.findAll();

        List<Dtos> dtos = new ArrayList<>();
        for (Entitis entitis1: entitis) {
            dtos.add(modelMapper.map(entitis1, Dtos.class));
        }
        return dtos;
    }
}
