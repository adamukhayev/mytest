package com.example.demo.controller;

import com.example.demo.model.dto.Dtos;
import com.example.demo.service.TaxService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(name = "/")
public class Controller {

    private final TaxService taxService;

    @GetMapping(path = "/get")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Object> get(@RequestParam(value = "row", required = false) String row,
                                      @RequestParam(value = "col",required = false) String col) {
       List<Dtos> taxDto = taxService.getTax(row,col);

        return ResponseEntity.ok(taxDto);
    }



}
