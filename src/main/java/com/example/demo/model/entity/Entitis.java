package com.example.demo.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Table(name = "tax")
public class Entitis {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "tax_group", nullable = false)
    private String taxGroup ;

    @Column(name = "tax_subgroup", nullable = false)
    private String taxSubgroup ;

    @Column(name = "district_of_the_Russian_Federation", nullable = false)
    private String districtOfTheRussianFederation;

    @Column(name = "region_of_Russian_Federation", nullable = false)
    private String regionOfRussianFederation ;

    @Column(name = "year", nullable = false)
    private Integer year;

    @Column(name = "value", nullable = false)
    private Integer value;
}
